@extends('layouts.app')

@section('style')
<style>
.page-content { margin-top:100px;}
body{background: #fafcff;}
.form-group-area { margin: 0 20%;}
.form-group-area label{ text-align:right;}
.card-header {  background-color: #007bff;
    color: #fff;
    font-size: 18px;
    text-transform: uppercase; }
	@media(max-width:767px){
	.form-group-area {
    margin: 0 0%;
}
.form-group-area label{ text-align:left;}
	}
</style>
@endsection

@section('content')
<div class="page-content">
<div class="row col-md-12">
    <div class="form-group float-left">
        <nav>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{url('bonuses/assign-user-list/all')}}">Assign Users</a></li>
              <li class="breadcrumb-item"><a href="#">Create</a></li>
          </ol>
        </nav>
      </div>
      </div>
    <div class="row justify-content-center">
        <div class="col-md-12">
		<div class="card-header">Assign Bonus to User</div>
            <div class="card">
                  <div class="form-group-area">

                <div class="card-body">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <form method="POST" action="{{url('bonuses/assign-user/'.$bonus->id)}}">
                    @csrf
                    <div class="form-group">
                        <div class="row">
                            <label for="bonus_id" class="col-sm-4">Bonus</label>
								<div class="col-sm-8">
                            <input type="text" readonly class="form-control" value="{{$bonus->name}}">
                        </div>
						                        </div>
                    </div>

                    <div class="form-group">
					  <div class="row">
					  <label  class="col-sm-4">Select User</label>
					  <div class="col-sm-8">
                        <select name="user_id" class="custom-select">
                            <option selected>Select User</option>
                            <option value="1">User 1</option>
                            <option value="2">User 2</option>
                            <option value="3">User 3</option>
                            <option value="4">User 4</option>
                            <option value="5">User 5</option>
                            <option value="6">User 6</option>
                            <option value="7">User 7</option>
                            <option value="8">User 8</option>
                            <option value="9">User 9</option>
                            <option value="10">User 10</option>
                        </select>
                    </div>
					 </div></div>

                    <div class="form-group">
					  <div class="row">
                        <label for="amount" class="col-sm-4">Amount(€)</label>
						 <div class="col-sm-8">
                        <input type="number" name="amount" class="form-control" placeholder="Amount">
                    </div>
 </div></div>
 
 <div class="form-group">
					<div class="row">
                        <label for="message" class="col-sm-4"></label>
						<div class="col-sm-8">
                               <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
					 </div>
					  </div>
                  
                    </form>
                </div>
            </div>
        </div>
		    </div>
    </div>
</div>
@endsection