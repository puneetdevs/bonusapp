@extends('layouts.app')

@section('style')
<style>
.page-content { margin-top:100px;}
body{background: #fafcff;}
.form-group-area { margin: 0 20%;}
.form-group-area label{ text-align:right;}
.card-header {  background-color: #007bff;
    color: #fff;
    font-size: 18px;
    text-transform: uppercase; }
	@media(max-width:767px){
	.form-group-area {
    margin: 0 0%;
}
.form-group-area label{ text-align:left;}
	}
</style>
@endsection

@section('content')
<div class="page-content">
<div class="row col-md-12">
    <div class="form-group float-left">
        <nav>
          <ol class="breadcrumb">
              <li class="breadcrumb-item"><a href="{{url('bonuses')}}">Bonus</a></li>
              <li class="breadcrumb-item"><a href="#">Edit Bonus</a></li>
          </ol>
        </nav>
      </div>
      </div>
    <div class="row justify-content-center">
           <div class="col-md-12">
		  <div class="card-header">Edit Bonus Form</div>
           <div class="card card-custom">
               <div class="form-group-area">

                <div class="card-body">
                @if($errors->all())
<div class="alert alert-danger alert-custom">
<ul>
@foreach ($errors->all() as $error)
<li>{{ $error }}</li>
@endforeach
</ul>
</div>
@endif
                <form method="POST" action="{{url('bonuses/'.$bonus->id)}}">
                    @csrf
                    {{ method_field('PATCH') }}
					 <div class="form-group">
					<div class="row">
                   
                        <label for="name" class="col-sm-4">Name</label>
						<div class="col-sm-8">
                        <input type="text" name="name" value="{{old('name', $bonus->name)}}" class="form-control" placeholder="Name">
                    </div>
					</div>
					  </div>
					  
 <div class="form-group">
					<div class="row">
                   
                        <label for="name_invoice" class="col-sm-4">Name Invoice</label>
						<div class="col-sm-8">
                        <input type="text" name="name_invoice" value="{{old('name_invoice', $bonus->name_invoice)}}" class="form-control" placeholder="Name Invoice">
                    </div>
					  </div>
					    </div>
						
 <div class="form-group">
					<div class="row">
                   
                        <label for="date_start" class="col-sm-4">Start Date</label>
						<div class="col-sm-8">
                        <input type="text" name="date_start" value="{{old('date_start', $bonus->date_start)}}" class="form-control" id="start_date">
                    </div>
					 </div>
					    </div>
 <div class="form-group">
					<div class="row">
                 
                        <label for="date_end" class="col-sm-4">End Date</label>
						<div class="col-sm-8">
                        <input type="text" name="date_end" value="{{old('date_end', $bonus->date_end)}}" class="form-control" id="end_date">
                    </div>
					</div></div>
 <div class="form-group">
					<div class="row">
               
                        <label for="amount" class="col-sm-4">Amount(€)</label>
						<div class="col-sm-8">
                        <input type="number" name="amount" value="{{old('amount', $bonus->amount)}}" class="form-control" placeholder="Amount">
                    </div>
					</div></div>
 <div class="form-group">
					<div class="row">
                  
                        <label for="message" class="col-sm-4">Message</label>
						<div class="col-sm-8">
                        <textarea class="form-control" name="message" rows="3">{{old('message', $bonus->message)}}</textarea>
                    </div>
					  </div>
					    </div>
					
				
					
               
					
					<div class="form-group">
					<div class="row">
                        <label for="message" class="col-sm-4"></label>
						<div class="col-sm-8">
                              <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
					 </div>
					  </div>
                    </form>
                </div>
            </div>
			 </div>
        </div>
    </div>
</div>
<script>
    $('#start_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });

    $('#end_date').datepicker({
        uiLibrary: 'bootstrap4',
        format: 'yyyy-mm-dd'
    });
</script>
@endsection