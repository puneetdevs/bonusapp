@extends('layouts.app')

@section('style')
<style>

body{background: #fafcff;}
h4 {
  margin: 2rem 0rem 1rem;
}
.table-box{       background: #fff;
    border-radius: 3px;
    box-shadow: 0 3px 2px 0 rgba(1,74,143,0.1);
    overflow-x: auto;}
	.table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
    background: #007bff;
    color: #fff;
    font-weight: normal;
}
.table { margin-bottom:0;}
.page-content { margin-top:100px;}
.table td, .table th {
    padding: 10px 10px;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}
	
.table-image {
  td, th {
    vertical-align: middle;
  }
}
@media(max-width:767px){
		.page-head h3{ font-size: 16px;
		margin-top: 8px;}
		.table td, .table th {
    padding: 10px 10px;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
    white-space: nowrap;
}
}
</style>
@endsection

@section('content')
<div class="page-content">
  <div class="row">
    <div class="col-12">
   
	  
	  <div class="page-head mb-3">
	    <div class="row">
		 <div class="col-6">
		 <h3>Assign User</h3>
		 </div>
    <div class="col-2">
	  <div class="float-right">
        <a type="submit" href="{{url('bonuses')}}" class="btn btn-primary">Bonus List</a>     
      </div>
	    </div>
      <div class="col-sm-4">
            <select class="custom-select button btn btn-primary" id="filter">
                <option value="0" selected>Filter By Bonus Name</option>
                <option {{($bonus_id) && $bonus_id == 'all' ? 'selected' : ''}} value="all">All</option>
                @foreach($bonuses as $bonus)
                    <option {{ ($bonus_id) && $bonus->bonus_id == $bonus_id ? 'selected' : ''}} value="{{$bonus->id}}">{{$bonus->name}}</option>
                @endforeach
            </select>
          </div>
		 </div>
	    </div>
	    <div class="clearfix"></div>
     <div class="table-box">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">Username</th>
            <th scope="col">Bonus</th>
            <th scope="col">Amount</th>
            <th scope="col">Entry Date</th>
          </tr>
        </thead>
        <tbody>
          @foreach($bonusUsers as $bonusUser)
          <tr>
            <th scope="row">{{$bonusUser->user()}}</th>
            <td>{{$bonusUser->bonus->name}}</td>
            <td>€{{!empty($bonusUser->amount) ? $bonusUser->amount : $bonusUser->bonus->amount}}</td>
            <td>{{date('Y-m-d', strtotime($bonusUser->date_entry))}}</td>
          </tr>
          @endforeach
        </tbody>
        @if(count($bonusUsers) == 0)
          <tr><td colspan="4" style="text-align:center">No Record found</td></tr>
        @endif
      </table>
    </div> </div>
  </div>
</div>
@endsection