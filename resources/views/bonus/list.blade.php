@extends('layouts.app')

@section('style')
<style>

body{background: #fafcff;}
h4 {
  margin: 2rem 0rem 1rem;
}
.table-box{       background: #fff;
    border-radius: 3px;
    box-shadow: 0 3px 2px 0 rgba(1,74,143,0.1);
    overflow-x: auto;}
	.table thead th {
    vertical-align: bottom;
    border-bottom: 2px solid #dee2e6;
    background: #007bff;
    color: #fff;
    font-weight: normal;
}
.table { margin-bottom:0;}
.page-content { margin-top:100px;}
.table td, .table th {
    padding: 10px 10px;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
}
	
.table-image {
  td, th {
    vertical-align: middle;
  }
}

	@media(max-width:767px){
		.page-head h3{ font-size: 16px;
		margin-top: 8px;}
		.table td, .table th {
    padding: 10px 10px;
    vertical-align: top;
    border-top: 1px solid #dee2e6;
    white-space: nowrap;
}
	}
</style>
@endsection

@section('content')
<div class="page-content">

  <div class="row">
    <div class="col-12">
      <div class="page-head mb-3">
	    <div class="row">
		 <div class="col-6">
		 <h3>Manage Bonus</h3>
		 </div>
    <div class="col-6">
	  <div class="float-right">
        <a type="submit" href="{{url('bonuses/create')}}" class="btn btn-primary">Create Bonus</a>      
      </div>
	    </div>
		 </div>
	    </div>
	  <div class="clearfix"></div>
	  <div class="table-box">
      <table class="table table-striped table-hover">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Amount</th>
            <th scope="col">Start Date</th>
            <th scope="col">End Date</th>
            <th scope="col">Entry Date</th>
            <th scope="col">Message</th>
            <th scope="col">Users</th>
            <th scope="col">Actions</th>
          </tr>
        </thead>
        <tbody>
            @foreach($bonuses as $index => $bonus)
          <tr>
            <th scope="row">{{$index+1}}</th>
            <td>{{$bonus->name}}</td>
            <td>€{{$bonus->amount}}</td>
            <td>{{$bonus->date_start}}</td>
            <td>{{$bonus->date_end}}</td>
            <td>{{date('Y-m-d', strtotime($bonus->date_entry))}}</td>
            <td>{{$bonus->message}}</td>
            @if($bonus->bonusUser)
            <td class="text-center"><a type="button" class="btn btn-warning btn-sm" href="{{url('bonuses/assign-user-list/'.$bonus->id)}}">{{$bonus->bonusUser->count()}}</td>
            @else
            <td class="text-center">No User Assigned</td>
            @endif
            <td>
                <a type="button" href="{{url('bonuses/'.$bonus->id.'/edit')}}" class="btn btn-primary btn-sm">Edit</a>
                <a type="button" class="btn btn-success btn-sm" href="{{url('bonuses/assign-user/'.$bonus->id)}}">Assign</a>
            </td>
          </tr>
          @endforeach
        </tbody>
      </table>
	  </div>
    </div>
  </div>
</div>

@endsection