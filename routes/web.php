<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('bonuses');
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('bonuses/assign-user/{bonus_id}', 'BonusController@assignUserForm');

Route::post('bonuses/assign-user/{bonus_id}', 'BonusController@assignUserPost');

Route::get('/bonuses/assign-user-list/{bonus_id}', 'BonusController@assignUserList');

Route::resource('bonuses', 'BonusController');
