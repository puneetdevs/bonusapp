<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Bonus;
use App\BonusUser;
use App\Http\Requests\BonusStoreRequest;
use App\Http\Requests\BonusUpdateRequest;
use Carbon\Carbon;

class BonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $bonuses = Bonus::orderby('date_entry', 'DESC')->get();

        return view('bonus.list')->with('bonuses', $bonuses);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('bonus.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\BonusStoreRequest;  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BonusStoreRequest $request)
    {
        $request->merge([
            'date_entry' => Carbon::now()
        ]);
        $bonus = Bonus::create($request->input());

        return redirect('/bonuses')->with('success', 'Bonus saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $bonus = Bonus::find($id);

        return view('bonus.edit')->with('bonus', $bonus);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(BonusUpdateRequest $request, $id)
    {
        $bonus = Bonus::find($id);

        $bonus->update($request->input());

        return redirect('/bonuses')->with('success', 'Bonus updated!');
    }

    public function assignUserForm($bonus_id)
    {
        $bonus = Bonus::find($bonus_id);

        return view('bonus.assign_user')->with('bonus', $bonus);
    }

    public function assignUserPost(Request $request, $bonus_id)
    {
        $request->validate([
            'user_id' => 'required|unique:bonus_user,user_id,NULL,id,bonus_id,'.$bonus_id,
        ], [
            'user_id.unique' => 'The Bonus has already been assigned to the selected User'
        ]);
        $bonus = Bonus::find($bonus_id);

        $startDate = Carbon::createFromFormat('Y-m-d',$bonus->date_start);
        $endDate = Carbon::createFromFormat('Y-m-d',$bonus->date_end);

        $check = Carbon::now()->between($startDate,$endDate);

        if(!$check){
            return redirect()->back()->withErrors(["Today's date does not come between start and end date of bonus"]);
        }

        $request->merge([
            'date_entry' => Carbon::now(),
            'bonus_id' => $bonus_id
        ]);

        BonusUser::create($request->input());

        return redirect('/bonuses');
    }

    public function assignUserList($bonus_id)
    {
        $bonuses = Bonus::get();

        if($bonus_id == 'all' || $bonus_id == '0'){
            $bonusUsers = BonusUser::get();
        }else{
            $bonusUsers = BonusUser::where('bonus_id', $bonus_id)->get();
        }

        return view('bonus.assign_user_list')->with('bonusUsers', $bonusUsers)->with('bonuses', $bonuses)->with('bonus_id', $bonus_id);
    }
}
