<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BonusUser extends Model
{
    protected $table = 'bonus_user';

    protected $fillable = [
        'user_id',
        'bonus_id',
        'date_entry',
        'amount',
    ];

    public $timestamps = false;

    private $users = [
        '1' => 'User 1',
        '2' => 'User 2',
        '3' => 'User 3',
        '4' => 'User 4',
        '5' => 'User 5',
        '6' => 'User 6',
        '7' => 'User 7',
        '8' => 'User 8',
        '9' => 'User 9',
        '10' => 'User 10',
    ];

    public function bonus(){
        return $this->belongsTo('App\Bonus', 'bonus_id', 'id');
    }

    public function user(){
        return $this->users[$this->user_id];
    }
}
