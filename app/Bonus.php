<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bonus extends Model
{
    protected $table = 'bonus_main';

    protected $fillable = [
        'name',
        'name_invoice',
        'date_entry',
        'date_start',
        'date_end',
        'amount',
        'message'
    ];

    public $timestamps = false;

    public function bonusUser(){
        return $this->hasMany('App\BonusUser', 'bonus_id', 'id');
    }
}
